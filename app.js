var t2k = angular.module("top2000", []);

t2k.directive('backImg', function () {
    return function (scope, element, attrs) {
        var url = scope.$eval(attrs.backImg);
        
        if (String(url).length == 0) {
            url = "https://www.nporadio2.nl/images/header-decoration.webp";
        }
        
        element.css({
            'background-image': 'url(' + url + ')'
        });

        scope.$watch(attrs.backImg, function (newValue, oldValue) {
            
            var url = "https://www.nporadio2.nl/images/header-decoration.webp";

            if (newValue) {
                url = scope.$eval(attrs.backImg);
            }

            element.css({
                'background-image': 'url(' + url + ')'
            });
        });
    };
});

t2k.filter('abs', function () {
    return function (val) {
        return Math.abs(val);
    };
});

t2k.factory('audio', function ($document) {
    var audioElement = $document[0].createElement('audio');
    return {
        audioElement: audioElement,

        play: function (filename) {
            audioElement.src = filename;
            audioElement.play();
        },

        stop: function () {
            audioElement.pause();
            audioElement.src = '';
        },

        volume: function (vol) {
            if (vol > 1) { vol = 1; }
            if (vol < 0) { vol = 0; }

            audioElement.volume = vol;
        }
    };
});


t2k.factory("notification", function() {
    //Implementation of Notification
    var proxy = function(title, message, icon) {
        try{
            var options = {
                body: message,
                icon: icon
            }
            var notification = new Notification(title, options);
        } catch (err) {
            //Do nothing
        }
    };

    //First set the noop
    var show = function(){};

    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
        //Nothing to do
    }
    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
        show = proxy;
    }
    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                show = proxy;
            }
        });
    }

    return {
        show: show
    };
});


t2k.controller("ticker", function ($http, $scope, $filter, $timeout, $interval, $filter, audio, notification) {

    var body = angular.element(document.body);
    var backgroundSize = 'cover';

    var replaceImgDimensions = /(w_\d+,h_\d+)/;

    $scope.artist = null;
    $scope.title = null;
    $scope.image = '';
    $scope.id = null;
    $scope.track = null;
    $scope.chart_tracks = [];

    $scope.startstream = function () {
        audio.play("//icecast.omroep.nl/radio2-bb-mp3");
    };

    $scope.stopstream = function () {
        audio.stop();
    };

    $scope.volumeUp = function () {
        audio.volume(audio.audioElement.volume + 0.05);
    };

    $scope.volumeDown = function () {
        audio.volume(audio.audioElement.volume - 0.05);
    };

    $scope.search = function(term) {
        if (term && term.length >= 1) {
            return $filter('filter')($scope.chart_tracks, term);
        }
        else
        {
            return null;
        } 
    };

    $scope.toggleBackground = function() {
        backgroundSize = backgroundSize == 'cover'? 'contain':'cover';
        body.css({'background-size': backgroundSize});
    };

    var onair_url = "https://www.nporadio2.nl/api/miniplayer/info?channel=npo-radio-2";
    var recent_url = "https://www.nporadio2.nl/api/tracks";
	var chart_url = (new Date()).getFullYear() + ".json"; // This data is a cached version of https://www.nporadio2.nl/api/charts/npo-radio-2-top-2000-van-2024-12-25

	(function download_playlist(){
		var due = new Date();
		due.setDate(25);
		due.setMonth(11);
		due.setHours(0);
		due.setMinutes(0);
		due.setSeconds(0);

    	now = new Date();

		if (now > due)
		{
			$http.get(chart_url).then(function(response){
				$scope.chart_tracks = _.reverse(response.data.positions);
				$timeout(extendsong.bind(this));
			});
		}
	})();

    var onair = function ($data) {
        $timeout(function () {
            // Get the current playing data
            $current = $data.data.data.radioTrackPlays.data[0];

            // Get the current track if available
            $track = $current.radioTracks;


            if ([$scope.artist, $scope.title].toString() != [$current.artist, $current.song].toString()) {
                $scope.artist = $current.artist;
                $scope.title = $current.song;
                $scope.image = '';
                $scope.id = null;
                $scope.cover = ''
                $scope.track = null;

                notification.show("Radio 2", "Now playing: " + $scope.artist + " - " + $scope.title, $scope.image);

                if ($track) {
                    $scope.image = $track.coverUrl;
                    $scope.id = $track.id;
                    $scope.cover = $track.coverUrl;
                }

                // Check if we have a chart position
                var position = ($current.cmsChartEditionPositions || {}).position;

                if (position)
                    extendsong(parseInt(position));
                else
                    extendsong();
            }
        });
    };

    var recent = function ($data) {
        $timeout(function () {
            $scope.recent = $filter('limitTo')($data.data.data, 10);
        });
    };

	var extendsong = function($position) {
        
        $id = $scope.id;
        $song = $scope.title;
        $artist = $scope.artist;

        // Search based on explicit position
        if ($position) {
            var seq = _($scope.chart_tracks).dropWhile(function(el) {
                return el.position.current > $position;
            }).take(11).value();

            if (seq.length > 0){
                updateTrack(seq[0], _.drop(seq));
                return;
            }
        }

        // Search based on track id
        if ($id) {
            // search in the reversed list for the current song
            var seq = _($scope.chart_tracks)
			.dropWhile(function(el)
            {
                // Do not use explicit checks as $id is an integer and el.aid is a string
                return el.id != $id;
            })
			.take(11) 
			.value();
            
            if (seq.length > 0){
                updateTrack(seq[0], _.drop(seq));
                return;
            }
        }
        
        // Otherwise try to match with the artist and title
        // search in the reversed list for the current song
        var seq = _($scope.chart_tracks)
        .dropWhile(function(el)
            {
                // Do a match on artist and song
                return ! (el.title == $song && el.artist == $artist);
            })
        .take(11) 
        .value();

        if (seq.length > 0){
            console.log('Matched song on title and artist');
			updateTrack(seq[0], _.drop(seq));
        }
    };
    
    var updateTrack = function($track, $list) {
        $scope.track = $track;
        $scope.upcoming = $list;
        $scope.position = $track.position;

        // Update the cover art with an upscaled version ofthe image
        $scope.image = fixDimensions($track.track.coverUrl || '');
        $scope.cover = $scope.image;
    };

    var fixDimensions = function($url) {
        return $url.replace(replaceImgDimensions, 'w_1200,h_1200' );
    }

    // Call the update function
    var update = function () {
        $http.get(onair_url).then(onair);
        $http.get(recent_url).then(recent);
    };

	$interval(update, 30000);

    update();
});