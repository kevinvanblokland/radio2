﻿var t2k = angular.module("top2000", ["ng-chrome-cast-channels"]);

t2k.directive('backImg', function () {
    return function (scope, element, attrs) {
        var url = scope.$eval(attrs.backImg);
                
        if (String(url).length == 0) {
            url = "https://radioplayer.npo.nl/player_assets/a4139c61f89728c0af9f/static/media/header-background.3f73a08a.png";
        }
        
        element.css({
            'background-image': 'url(' + url + ')'
        });

        scope.$watch(attrs.backImg, function (newValue, oldValue) {
            
            var url = "https://radioplayer.npo.nl/player_assets/a4139c61f89728c0af9f/static/media/header-background.3f73a08a.png";

            if (newValue) {
                url = scope.$eval(attrs.backImg);
            }

            element.css({
                'background-image': 'url(' + url + ')'
            });
        });
    };
});

t2k.filter('abs', function () {
    return function (val) {
        return Math.abs(val);
    };
});

t2k.factory('audio', function ($document) {
    var audioElement = $document[0].createElement('audio');
    return {
        audioElement: audioElement,

        play: function (filename) {
            audioElement.src = filename;
            audioElement.play();
        },

        stop: function () {
            audioElement.pause();
            audioElement.src = '';
        },

        volume: function (vol) {
            if (vol > 1) { vol = 1; }
            if (vol < 0) { vol = 0; }

            audioElement.volume = vol;
        }
    };
});


t2k.factory("notification", function() {
    //Implementation of Notification
    var proxy = function(title, message, icon) {
        try{
            var options = {
                body: message,
                icon: icon
            }
            var notification = new Notification(title, options);
        } catch (err) {
            //Do nothing
        }
    };

    //First set the noop
    var show = function(){};

    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
        //Nothing to do
    }
    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
        show = proxy;
    }
    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                show = proxy;
            }
        });
    }

    return {
        show: show
    };
});

t2k.controller("ticker", function ($http, $scope, $timeout, $interval, $filter, audio, notification, ChromeCastReceiverChannel) {

	// Initialize the receiver channel
	ChromeCastReceiverChannel.initialize();

    // Initialize the media element
    var mediaManager = new cast.receiver.MediaManager(audio.audioElement);
    var body = angular.element(document.body);
    var backgroundSize = 'cover';

    var replaceImgDimensions = /(w_\d+,h_\d+)/;

    $scope.artist = null;
    $scope.title = null;
    $scope.image = '';
    $scope.id = null;
    $scope.track = null;
    $scope.chart_tracks = [];

    $scope.startstream = function () {
        audio.play("//icecast.omroep.nl/radio2-bb-mp3");
    };

    $scope.stopstream = function () {
        audio.stop();
    };

    $scope.volumeUp = function () {
        audio.volume(audio.audioElement.volume + 0.05);
    };

    $scope.volumeDown = function () {
        audio.volume(audio.audioElement.volume - 0.05);
    };

    $scope.search = function(term) {
        if (term && term.length >= 3) {
            return $filter('filter')($scope.chart_tracks, term);
        }
        else
        {
            return null;
        }
        
    };

    $scope.startstream();

    var onair_url = "https://bloklab-radio2-proxy.azurewebsites.net/nowplaying"; // This url is a proxy url for https://www.nporadio2.nl/?option=com_ajax&plugin=nowplaying&format=json&channel=nporadio2
    var recent_url = "https://www.nporadio2.nl/api/tracks";
	var chart_url = (new Date()).getFullYear() + ".json"; // This data is a cached verison of https://www.nporadio2.nl/index.php?option=com_ajax&plugin=Top2000&format=json&year=2020we&npo_cc_skip_wall=1

    (function download_playlist(){
		var due = new Date();
		due.setDate(25);
		due.setMonth(11);
		due.setHours(0);
		due.setMinutes(0);
		due.setSeconds(0);

        now = new Date();

		if (now > due)
		{
			$http.get(chart_url).then(function(response){
				$scope.chart_tracks = _.reverse(response.data.data[0]);
				$timeout(extendsong.bind(this, $scope));
			});
		}
	})();

    var onair = function ($data) {
        $timeout(function () {
            $current = $data.data.data[0];

            // $scope.songfile = $songfile;
            // $scope.expires = $current.stopdatetime;
            // $scope.start = new Date($current.startdatetime);
            // $scope.stop = new Date($current.stopdatetime);
            // $scope.duration = new Date($scope.stop.getTime() - $scope.start.getTime());
            // $scope.length = $scope.stop - $scope.start;

            // Show alert if a new song is being played
            if ([$scope.id, $scope.title, $scope.artist].toString() !== [$current.id, $current.title, $current.artist].toString()) {
                
                $scope.id = $current.id;
                $scope.artist = $current.artist;
                $scope.title = $current.title;
                $scope.image = $current.image;
                $scope.cover = $current.image;
                $scope.track = null;

				// Extend the song information for the top 2000 chart
            	extendsong($current);

                notification.show("Radio 2", "Now playing: " + $scope.artist + " - " + $scope.title, $scope.image);
            }
        });
    };

    var recent = function ($data) {
        $timeout(function () {
            $scope.recent = $filter('limitTo')($data.data.data, 10);
        });
    };

	var extendsong = function($current) {
        
        let $id = $current.id;
        let $song = $current.title;
        let $artist = $current.artist;

        if ($current === undefined) {
            return;
        }

		// search in the reversed list for the current song
        var seq = _($scope.chart_tracks)
			.dropWhile(function(el)
				{
                    // Do not use explicit checks as $id is an integer and el.aid is a string
					return el.aid != $id;
				})
			.take(11) 
			.value();

		if (seq.length > 0){
			updateTrack(seq[0], _.drop(seq));
            return;
        }
        
        // Otherwise try to match with the artist and title
        // search in the reversed list for the current song
        var seq = _($scope.chart_tracks)
        .dropWhile(function(el)
            {
                // Do a match on artist and song
                return ! (el.s == $song && el.a == $artist);
            })
        .take(11) 
        .value();

        if (seq.length > 0){
            console.log('Matched song on title and artist');
			updateTrack(seq[0], _.drop(seq));
        }
    };

    var updateTrack = function($track, $list) {
        $scope.track = $track;
        $scope.upcoming = $list;

        // Update the cover art with an upscaled version ofthe image
        $scope.image = fixDimensions($track.img || '');
        $scope.cover = $scope.image;
    };

    var fixDimensions = function($url) {
        return $url.replace(replaceImgDimensions, 'w_1200,h_1200' );
    }

    // Call the update function
    var update = function () {
        $http.get(onair_url).then(onair);
        $http.get(recent_url).then(recent);
    };

    $interval(update, 30000);

    update();
});